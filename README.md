# WiiU-Title-Key
## Title Key Database for WiiU

### Installation setup (Linux/Windows/MacOS):
- Create a database locally named homestead utf8_general_ci.
- Rename .env.example file to .envinside your project root and fill the database information (Windows wont let you do it, so you have to open your console cd your project root directory and run mv .env.example .env).
- Now open a terminal inside your project root directory and run the following commands.
```bash
git clone https://github.com/Natizyskunk/WiiU-Title-Key
cd WiiU-Title-Key
composer install
npm install
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan serve
```
You can now access your project at localhost:8000 :)

[Setup laravel project (local)](https://gist.github.com/Natizyskunk/bcdae6acc5c218e6eb75f1c61b5f6072)
